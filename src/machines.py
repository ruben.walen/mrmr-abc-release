import random
import numpy as np
import copy
import sklearn.svm

import config

class FSArtificialBeeColony:
    def __init__(self, n_bees_employed, n_bees_onlooker, n_cycles_abandon, vec_length, vec_counts, evaluator, flip_probability_control):
        if (sum(vec_counts) != vec_length):
            print("Construction of FSArtificialBeeColony failed: vec_counts does not match vec_length")
            exit
        
        self.n_emp = n_bees_employed # number of employee bees
        self.n_onl = n_bees_onlooker # number of onlooker bees
        self.abandon_on = n_cycles_abandon # bees abandon a solution after this many cycles of non-improvement

        self.vec_length = vec_length # dimensionality
        self.vec_counts = vec_counts # 1, 0 counts

        self.evaluator = evaluator # provide a partial with a vector as input which can return a (numeric) fitness value

        self.fp = flip_probability_control # scales number of flips in mutation (step size): at least one flip always occurs, but this controls the probability of more flips per mutation

        # initialise
        self.emp_solutions = np.zeros((n_bees_employed, vec_length))
        self.emp_evaluations = np.zeros((n_bees_employed))
        self.onl_solutions = np.zeros((n_bees_onlooker, vec_length))

        self.best_so_far_fitness = -1
        self.best_so_far_solution_emp_index = -1
        
        for e in range(n_bees_employed):
            init_vector = np.array(generateFeatureVector(vec_length, vec_counts))
            self.emp_solutions[e, :] = init_vector
            fit = self.evaluateSolution(init_vector)
            self.emp_evaluations[e] = fit
            if (fit > self.best_so_far_fitness):
                self.best_so_far_fitness = fit
                self.best_so_far_solution_emp_index = e

        self.cycles_emp = np.zeros((n_bees_employed))
        self.cycles_onl = np.zeros((n_bees_onlooker))
        self.onl_target_bee = np.full((n_bees_onlooker), -1) # onlooker bee targets

        self.done_status = False # control this yourself using termination conditions

    def evaluateSolution(self, vector): # evaluate a solution using the evaluator provided in the constructor as a partial: must return a numeric value (positive!)
        v = self.evaluator(vector)
        return v

    def proportionalIndexSelection(self, fitnesses): # requires positive numeric values; select an index with probability proportional to the fitness values
        copy_fitnesses = copy.copy(fitnesses)
        for f in range(1, np.shape(fitnesses)[0]):
            copy_fitnesses[f] += copy_fitnesses[f - 1]

        pick = random.random() * copy_fitnesses[-1]
        index = 0
        while ((index < (np.shape(fitnesses)[0] - 1)) and (copy_fitnesses[index] < pick)):
            index += 1
        return index

    def randomNFlips(self):
        nflips = 1
        while (random.random() < self.fp): # sequentially potential increases of number of flips
            nflips += 1
        return nflips

    def step(self): # perform one iteration
        if (self.done_status):
            print("FSArtificialBeeColony.step: Received step signal while done_status is True. Please ensure proper termination.")
        
        # employee bee phase
        for e in range(self.n_emp):
            if (self.cycles_emp[e] >= self.abandon_on): # become a scout bee, immediately become employed again (generate a new solution)
                init_vector = np.array(generateFeatureVector(self.vec_length, self.vec_counts))
                self.emp_solutions[e, :] = init_vector
                fit = self.evaluateSolution(init_vector)
                self.emp_evaluations[e] = fit
                self.cycles_emp[e] = 0
                if (fit > self.best_so_far_fitness): # best-so-far check
                    self.best_so_far_fitness = fit
                    self.best_so_far_solution_emp_index = e
            else: # still an employee
                mutated_vector = mutateNFlips(self.emp_solutions[e, :], self.randomNFlips(), self.vec_counts) # mutate known solution
                fit = self.evaluateSolution(mutated_vector)
                if (fit > self.emp_evaluations[e]): # found a better solution; upgrade and reset cycle count
                    self.emp_solutions[e, :] = mutated_vector
                    self.emp_evaluations[e] = fit
                    self.cycles_emp[e] = 0
                    if (fit > self.best_so_far_fitness): # best-so-far check
                        self.best_so_far_fitness = fit
                        self.best_so_far_solution_emp_index = e
                else: # we did not improve; increase cycle count
                    self.cycles_emp[e] += 1

        # onlooker bee phase
        for o in range(self.n_onl):
            if ((self.cycles_onl[o] >= self.abandon_on) or (self.onl_target_bee[o] == -1)): # become a scout bee, immediately become onlooker again to search for new solution
                place_to_bee = self.proportionalIndexSelection(self.emp_evaluations) # proportionally select an employee bee
            else:
                place_to_bee = self.onl_target_bee[o] # we still have a target

            mutated_vector = mutateNFlips(self.emp_solutions[place_to_bee, :], self.randomNFlips(), self.vec_counts) # mutate known solution
            fit = self.evaluateSolution(mutated_vector)
            if (fit > self.emp_evaluations[place_to_bee]): # onlooker found a better solution; upgrade and reset cycle count
                self.emp_solutions[place_to_bee, :] = mutated_vector
                self.emp_evaluations[place_to_bee] = fit
                self.cycles_emp[place_to_bee] = 0 # also reset the employee's cycle
                self.cycles_onl[o] = 0 # reset own cycle
                if (fit > self.best_so_far_fitness): # best-so-far check
                    self.best_so_far_fitness = fit
                    self.best_so_far_solution_emp_index = place_to_bee
            else:
                # nae bother with the employee's cycle
                self.cycles_onl[o] += 1        

class FSGeneticAlgorithm:
    def __init__(self, mu_population, selection_absolute_weight, p_crossover, crossover_npoints, vec_length, vec_counts, evaluator, mutation_flip_probability_control):
        if (sum(vec_counts) != vec_length):
            print("Construction of FSArtificialBeeColony failed: vec_counts does not match vec_length")
            exit
        
        self.mu = mu_population # population size
        self.sel_alpha = selection_absolute_weight # constant weight in proportional selection
        self.pc = p_crossover # crossover probability
        self.cnp = crossover_npoints # crossover number of points

        self.vec_length = vec_length # dimensionality
        self.vec_counts = vec_counts # 1, 0 counts

        self.evaluator = evaluator # provide a partial with a vector as input which can return a (numeric) fitness value

        self.pm = mutation_flip_probability_control # scales number of flips in mutation (step size): at least one flip always occurs, but this controls the probability of more flips per mutation

        # initialise
        self.solutions = np.zeros((self.mu, vec_length))
        self.evals = np.zeros((self.mu))

        self.best_so_far_fitness = -1
        self.best_so_far_solution_index = -1
        
        for e in range(self.mu):
            init_vector = np.array(generateFeatureVector(vec_length, vec_counts))
            self.solutions[e, :] = init_vector
            fit = self.evaluateSolution(init_vector)
            self.evals[e] = fit
            if (fit > self.best_so_far_fitness):
                self.best_so_far_fitness = fit
                self.best_so_far_solution_index = e

        self.done_status = False # control this yourself using termination conditions

    def evaluateSolution(self, vector): # evaluate a solution using the evaluator provided in the constructor as a partial: must return a numeric value (positive!)
        v = self.evaluator(vector)
        return v

    def proportionalIndexSelection(self, fitnesses, alpha): # requires positive numeric values; select an index with probability proportional to the fitness values
        copy_fitnesses = copy.copy(fitnesses)
        for f in range(1, np.shape(fitnesses)[0]):
            copy_fitnesses[f] += (copy_fitnesses[f - 1] + alpha)

        pick = random.random() * copy_fitnesses[-1]
        index = 0
        while ((index < (np.shape(fitnesses)[0] - 1)) and (copy_fitnesses[index] < pick)):
            index += 1
            
        return index

    def randomNFlips(self):
        nflips = 1
        while (random.random() < self.pm): # sequentially potential increases of number of flips
            nflips += 1
        return nflips

    def step(self): # perform one iteration
        if (self.done_status):
            print("FSGeneticAlgorithm.step: Received step signal while done_status is True. Please ensure proper termination.")
        
        # selection, crossover, mutation
        next_population = np.zeros((self.mu, self.vec_length))
        next_fitnesses = np.zeros((self.mu))
        for e in range(self.mu):
            selected_index = self.proportionalIndexSelection(self.evals, self.sel_alpha)
            if (random.random() < self.pc): # do crossover
                crossover_selected_index = self.proportionalIndexSelection(self.evals, self.sel_alpha)
                vector_prime = crossoverVectorsSimpleNCountConserve(self.solutions[selected_index, :], self.solutions[crossover_selected_index, :]) #self.cnp n points
            else: # don't do crossover
                vector_prime = self.solutions[selected_index, :]
            vector_prime2 = mutateNFlips(vector_prime, self.randomNFlips(), self.vec_counts)
            next_population[e, :] = vector_prime2
            fit = self.evaluateSolution(vector_prime2)
            next_fitnesses[e] = fit

            if (fit > self.best_so_far_fitness): # best fitness?
                self.best_so_far_fitness = fit
                self.best_so_far_solution_index = e
                
        self.solutions = next_population # replace population
        self.evals = next_fitnesses # replace fitnesses

def generateFeatureVector(length, counts): # generate a random bitstring with the given counts of 1s and 0s
    vector = []
    ncounts = counts
    total_count = counts[0] + counts[1]
    for _ in range(length):
        p_0 = ncounts[0] / total_count
        p_1 = ncounts[1] / total_count
        if (random.random() < p_0):
            ncounts = (ncounts[0] - 1, ncounts[1])
            vector.append(0)
        else:
            ncounts = (ncounts[0], ncounts[1] - 1)
            vector.append(1)
        total_count -= 1
    return vector

def mutateNFlips(vector, nflips, counts): # mutate a bitstring by flipping 2*nflips bits, but conserve the number of 1s and 0s
    for c in counts:
        if (nflips > c): # can't do that many flips!
            nflips = c
    
    new_vector = copy.copy(vector)
    uflips = [] # 0 --> 1
    dflips = [] # 1 --> 0
    for i in range(nflips): # draft indices to be flipped
        cont = False
        while (cont == False):
            index = 1 + int(random.random() * counts[0])
            if (index not in uflips):
                uflips.append(index)
                cont = True
        cont = False
        while (cont == False):
            index = 1 + int(random.random() * counts[1])
            if (index not in dflips):
                dflips.append(index)
                cont = True
                
    u_count = 0 # how many 0s so far
    d_count = 0 # how many 1s so far
    for i in range(np.shape(vector)[0]): # do the flipping
        if (vector[i] == 1):
            d_count += 1
            if (d_count in dflips):
                new_vector[i] = 0
        else:
            u_count += 1
            if (u_count in uflips):
                new_vector[i] = 1
    return new_vector

def crossoverVectorsSimple(vector1, vector2, npoints): # do n-points bitwise crossover, no vector count conservation
    size = np.shape(vector1)[0]
    if (np.shape(vector1)[0] != np.shape(vector2)[0]):
        raise IndexError("crossoverFeatureVectors: vector shapes do not match")
    if (npoints >= size):
        print("crossoverFeatureVectors: too many crossover points, reducing")
        npoints = np.shape(vector1)[0] - 1

    positions = [i for i in range(size)]
    cross_positions = [] # positions to crossover: start crossover strand switch at that index
    for i in range(npoints):
        cp = random.choice(positions)
        cross_positions.append(cp)
        positions.pop(i) # index = value

    new_vector = np.zeros((size))
    active_vector1 = True # vector 1 is crossing, else switch to vector 2
    for i in range(size):
        if i in cross_positions: # improve efficiency?
            active_vector1 = [True, False][active_vector1 == True]
        if (active_vector1):
            new_vector[i] = vector1[i]
        else:
            new_vector[i] = vector2[i]

    return new_vector

# crossover, but conserve vector counts    
def crossoverVectorsSimpleNCountConserve(vector1, vector2): # do random bitwise crossover with vector count conservation
    size = np.shape(vector1)[0]
    if (np.shape(vector1)[0] != np.shape(vector2)[0]):
        raise IndexError("crossoverFeatureVectors: vector shapes do not match")

    new_vector = np.full((size), -1) # -1 should never occur; only if sth goes wrong
    for i in range(size):
        if (vector1[i] == vector2[i]): # 00 --> 0; 11 --> 1
            new_vector[i] = vector1[i]
        # 01 or 01 --> fill later

    list_01 = []
    list_10 = []
    for i in range(size): # conserve vector counts for unequal index positions
        if (vector1[i] > vector2[i]): # 10
            list_10.append(i)
        elif (vector2[i] > vector1[i]): # 01
            list_01.append(i)

    topick = len(list_10)
    count_left = topick
    for i in range(topick): # half the length; pick indices
        ind_01 = int(random.random() * count_left)
        ind_10 = int(random.random() * count_left)
        first = [0, 1][random.random() > 0.5]
        new_vector[list_01[ind_01]] = first
        new_vector[list_10[ind_10]] = 1 - first
        list_01.pop(ind_01)
        list_10.pop(ind_10)
        count_left -= 1

    return new_vector

##def crossoverVectorsSimpleNCountConserveNPoints(vector1, vector2, npoints): # do n-points bitwise crossover with vector count conservation
##    size = np.shape(vector1)[0]
##    if (np.shape(vector1)[0] != np.shape(vector2)[0]):
##        raise IndexError("crossoverFeatureVectors: vector shapes do not match")
##    if (npoints >= size):
##        print("crossoverFeatureVectors: too many crossover points, reducing")
##        npoints = np.shape(vector1)[0] - 1
##
##    positions = [i for i in range(size)]
##    cross_positions = [] # positions to crossover: start crossover strand switch at that index
##    for i in range(npoints):
##        cp = random.choice(positions)
##        cross_positions.append(cp)
##        positions.pop(i) # index = value
##
##    new_vector = np.zeros((size))
##    active_vector1 = True # vector 1 is crossing, else switch to vector 2; over whole vector
##    for i in range(size):
##        if i in cross_positions: # improve efficiency?
##            active_vector1 = [True, False][active_vector1 == True]
##            
##        if (vector1[i] == vector2[i]): # 00 --> 0; 11 --> 1
##            new_vector[i] = vector1[i]
##        else: # 01 or 01 --> pick from active strand
##            new_vector[i] = [vector1[i], vector2[i]][active_vector1 == False]
##
##    return new_vector

# LOOCV cross-validation: K classes, class_counts dictionary number of samples per class, class_labels vector of classes per sample, loocv_ratio is an integer!!
# i.e. loocv_ratio = 5 --> 5 sets; 1/5 test; 4/5 training
# keep ratio of classes constant for each set
def getIndexSetsLOOCV(classes, class_counts, class_labels, loocv_ratio, return_sample_distr_info=False):
    num_sets = int(loocv_ratio) # throw TypeError
    set_share = 1 / num_sets
    samples_per_class = {}
    for c in classes:
        count = class_counts[c]
        spc = count * set_share
        remainder = count - int(spc) * num_sets
        if (remainder < 0):
            cdir = "Down"
        elif (remainder > 0):
            cdir = "Up"
        samples_per_class[c] = [int(spc) for _ in range(num_sets)] # equal samples per class
        for i in range(remainder): # remainder: some sets get minimally more samples of a certain class
            if (cdir == "Up"): # add a sample
                samples_per_class[c][i] += 1
            elif (cdir == "Down"): # subtract a sample
                samples_per_class[c][i] -= 1

    cumul_indices_per_set = {} # per class: per set, which cumulative (per class) indices of samples does that set get, taking into account class balancing
    for c in classes: # per class
        count = class_counts[c]
        spc = copy.copy(samples_per_class[c]) # copy protection: must copy
        c_spc = copy.copy(spc) # copy protection: must copy
        selects = [[] for _ in range(num_sets)]
        for i in range(1, len(spc)):
            c_spc[i] += c_spc[i - 1] # cumulative counts

        for i in range(count): # distribute individual cumulative indices
            draft = int(random.random() * c_spc[-1]) # proportional selection
            cont = False
            index = 0
            while ((cont == False) and (index < len(spc))):
                if (draft < c_spc[index]): # pick this index
                    cont = True
                else:
                    index += 1
            selects[index].append(i)
            spc[index] -= 1 # dynamically alter
            for j in range(index, num_sets): # dynamically alter
                c_spc[j] -= 1

        cumul_indices_per_set[c] = selects

    if (type(class_labels) == list or type(class_labels) == tuple): # handle numpy array or list, tuple
        length = len(class_labels)
    else:
        length = np.shape(class_labels)[0]

    cumul_counts = [0 for _ in classes] # cumulative counts
    class_numeric_indices = {}
    for i in range(len(classes)):
        class_numeric_indices[classes[i]] = i

    indices_per_set = [[] for _ in range(num_sets)]
    for i in range(0, length): # allocation
        c = class_labels[i]
        ci = class_numeric_indices[c]
        curr_count = cumul_counts[ci]
        for s in range(len(cumul_indices_per_set[c])):
            if curr_count in cumul_indices_per_set[c][s]: # this sample index belongs to this set
                indices_per_set[s].append(i)

        cumul_counts[ci] += 1

    if (return_sample_distr_info): # return additional information on sample/class distribution
        return (indices_per_set, cumul_indices_per_set, samples_per_class)
    else:
        return indices_per_set

# SVM classifier-related functions
def getAndFitSVM(X, y, **kwargs): # get and fit and SVM
    svm = sklearn.svm.SVC(**kwargs)
    svm.fit(X, y)
    return svm

def defaultPredictSVM(X_train, y_train, X_test, y_test, kernel='linear', also_predict_train=False): # binary basic prediction for given kernel; given accuracy, recall, precision (based on class '1')
    svm = getAndFitSVM(X_train, y_train, kernel=kernel, gamma='auto')
    y_pred = svm.predict(X_test)
    if (also_predict_train):
        y_pred_train = svm.predict(X_train)
        draft = ("train", "test")
        results = {}
    else:
        draft = ("test")
    
    for s in draft:
        if (s == "train"):
            X = X_train
            y = y_train
            y_p = y_pred_train
        else:
            X = X_test
            y = y_test
            y_p = y_pred

        #print(y, y_p) # see the prediction, you can output y_p if you need it
        length = np.shape(X)[0]
        diff = y_p - y
        FP = 0
        FN = 0
        TP = 0
        TN = 0
        for i in range(length):
            if (diff[i] == 0):
                if (y[i] == 1):
                    TP += 1 # true pos
                elif (y[i] == 0):
                    TN += 1 # true neg
            elif (diff[i] == -1):
                FN += 1 # false neg
            elif (diff[i] == 1):
                FP += 1 # false positive

        acc = (TP + TN) / length # accuracy
        prc = (TP) / (TP + FP) # precision
        rec = (TP) / (TP + FN) # recall
        if (also_predict_train):
            results[s] = (acc, prc, rec)
        else:
            results = (acc, prc, rec)

    return results

def defaultSVMObjectiveFunction(X_train, y_train, X_test, y_test, w_accuracy, w_precision, w_recall, kernel='linear'): # an objective (maximisation) function for use with ABC-SVM; use with the wrapper where you can provide gene_exp_array
    params = defaultPredictSVM(X_train, y_train, X_test, y_test, also_predict_train=False)
    score = w_accuracy * params[0] + w_precision * params[1] + w_recall * params[2] # score is weighted sum of accuracy, precision, recall of test set
    return score

def wrapperSVM_DOF(subset_selection, gene_exp_train, y_train, gene_exp_test, y_test, w_accuracy, w_precision, w_recall, kernel='linear'): # see above; can be used by the ABC (provide solution to subset_selection)
    indices = np.where(subset_selection == 1)[0]
    sub_gene_exp_array_train = gene_exp_train[indices, :].T # need to invert the axes?
    sub_gene_exp_array_test = gene_exp_test[indices, :].T
    score = defaultSVMObjectiveFunction(sub_gene_exp_array_train, y_train, sub_gene_exp_array_test, y_test, w_accuracy, w_precision, w_recall, kernel='linear')
    return score
    
