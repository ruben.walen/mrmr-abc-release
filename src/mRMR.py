import os
import statistics
import math
import random

import config

configuration = config.getConfig()
data_dir = os.getcwd() + "\\" + configuration["data_directory"]
paste_dir = os.getcwd() + "\\" + configuration["output_directory"]
work_dir = os.getcwd() + "\\" + configuration["working_directory"]
samples = configuration["sample_names"]

seed = configuration["random_seed"]
if (seed != "random"):
    random.seed(int(seed))

# functions
def F_statistic(classes, class_labels, gene_expression_data, samples_per_class=False): # computing the ANOVA equals-means F-statistic: gene relevancy; reduces to t^2 test in case of two classes
    K = len(classes) # how many classes
    mean_g = statistics.mean(gene_expression_data) # average of gene expression
    n = len(class_labels) # how many samples
    if (samples_per_class == False):
        n_k = [0 for _ in range(K)] # samples per class
        for c in range(K):
            for i in range(len(class_labels)):
                if (class_labels[i] == classes[c]):
                    n_k[c] += 1
    else:
        n_k = samples_per_class

    g_k = [0 for _ in range(K)] # class gene exp. averages
    for c in range(K):
        for i in range(len(class_labels)):
            if (class_labels[i] == classes[c]):
                g_k[c] += gene_expression_data[i]
        g_k[c] = g_k[c] / n_k[c]

    sigma_squared = 0 # estimated standard deviation (variance)
    sigma_squared_k = [0 for _ in range(K)] # estimated variance per class
    for c in range(K):
        for i in range(len(class_labels)):
            if (class_labels[i] == classes[c]):
                sigma_squared_k[c] += (gene_expression_data[i] - g_k[c])**2
        sigma_squared_k[c] = sigma_squared_k[c] / (n_k[c] - 1) # Bessel's unbiased correction
        sigma_squared += ((n_k[c] - 1) * sigma_squared_k[c])
    sigma_squared = sigma_squared / (n - K)

    F_statistic_value = 0
    for c in range(K):
        F_statistic_value += ((n_k[c] * (g_k[c] - mean_g)**2) / (K - 1)) # I think the **2 here was omitted from the mRMR GE paper in error
    F_statistic_value = F_statistic_value / sigma_squared

    return F_statistic_value

def Pearson_correlation(gene_exp_1, gene_exp_2, return_variances=False): # compute Pearson correlation between two gene expression value lists
    if (len(gene_exp_1) != len(gene_exp_2)):
        print("mRMR.Pearson_correlation: gene expression lengths do not match")
        exit
        
    x_mean = statistics.mean(gene_exp_1)
    y_mean = statistics.mean(gene_exp_2)

    cov_xy = 0
    var_x = 0
    var_y = 0
    for i in range(len(gene_exp_1)):
        v_x = (gene_exp_1[i] - x_mean)
        v_y = (gene_exp_2[i] - y_mean)
        cov_xy += (v_x * v_y)
        var_x += (v_x ** 2)
        var_y += (v_y ** 2)

    pcc = (cov_xy) / (math.sqrt(var_x) * math.sqrt(var_y)) # the coefficient value

    if (return_variances == True):
        return (pcc, cov_xy, var_x, var_y)
    else:
        return pcc

# mRMR computation, gene relevancy computation (output to file possible)     
for samp in samples:
    # retrieve metadata
    print(samp)
    try:
        metadata_file = open((work_dir + "\\" + samp + "_meta.txt"))
    except FileNotFoundError:
        print("Could not find metadata for:", samp, "\nFirst run extract.py with the correct configuration")
        exit
    metadata = config.processMetadata(config.readMetadata(metadata_file))
    metadata_file.close()

    # get the data
    directory = data_dir + "\\" + samp
    filename = os.listdir(directory)[0]
    file_handle = open((directory + "\\" + filename))
    file_parsed = []
    line_classification = []
    for line in file_handle: # handling SOFT data
        culled_line = line[0:-1]
        entry_type = culled_line.split(" = ")
        if (len(entry_type) > 1):
            line_classification.append(entry_type[0]) # metadata separated by "key = value"
        elif (culled_line[0] == "!"):
            line_classification.append(culled_line) # !dataset_table_begin
        else:
            line_classification.append("data") # body data
        file_parsed.append(culled_line.split("\t")) # the line itself
    file_handle.close()

    # get some indices, retrieve the classes
    sample_begin = 2
    class_labels_ordered = []
    for h in range(sample_begin, (len(metadata["HEADER"]) - sample_begin)):
        if ("GSM" not in metadata["HEADER"][h]):
            sample_end = h
            break
        else:
            class_labels_ordered.append(metadata["SAMPLE_TYPES"]["#" + metadata["HEADER"][h]])
            if (class_labels_ordered[-1] == "Unknown"):
                print("Unknown labelled samples detected. Please correct the data.")
                exit

    # go through the genes list; do relevancy computation (F-statistic)
    dtable_begin = metadata["DATA_INDICES"]["DATATABLE_BEGIN"]
    write_genes = open((paste_dir + "\\" + samp + "_genes.txt"), 'w')
    write_genes.write("Gene symbol,Gene identifier\n")
    class_numbers = (metadata["NSAMPLES"]["CANCER"], metadata["NSAMPLES"]["NORMAL"]) # this refers to the typical classes; it is not necessarily a cancer-non cancer distinction, just binary classes

    relevancy_list = []
    relevancy_indices_list_sorted = []
    largest_relevancy = 0
    largest_relevancy_index = -1
    get_all_relevancies = configuration["mrmr_output_all_relevancies"]
    do_compute_relevancies = False
    num_genes = metadata["DATA_INDICES"]["DATATABLE_END"] - (dtable_begin + 2)
    classes_names = configuration["classes"]
    classes_names_ids = configuration["class_ids"]
    
    if (configuration["mrmr_use_relevancies_file"] == True): # trying to find a gene relevancies file to skip the F-statistic processing, if applicable
        try:
            file_relevancies = open((work_dir + "\\" + samp + "_gene_relevancies.txt"))
        except FileNotFoundError:
            if (get_all_relevancies == False):
                print("Could not find a gene relevancies file. Make sure it exists by using mrmr_output_all_relevancies config option at least once.")
                exit
            else:
                print("Outputting all relevancies per gene. This may take a while.")
                do_compute_relevancies = True
    else:
        print("Computing relevancies only up to mrmr_number_of_genes limit") # this won't actually work with the mRMR selection, we need all relevancies
        do_compute_relevancies = True
    n_genes_to_select = configuration["mrmr_number_of_genes"]

    if (do_compute_relevancies == False):
        print("Skipping relevancy computation")

    print("Number of genes:", num_genes) 
    for g in range(dtable_begin + 2, metadata["DATA_INDICES"]["DATATABLE_END"]):
        content = file_parsed[g]
        write_genes.write(content[1] + "," + content[0] + '\n') # write a file containing all found genes + identifiers for the sample
        # gene identifier (some gene symbols have more than one identifier --> alternative splicing?)
        #gene_exp = [float(content[i]) for i in range(sample_begin, sample_end)]

        if (do_compute_relevancies and (g - (dtable_begin + 2)) % 1000 == 0):
            print("Index:", (g - (dtable_begin + 2)), "Gene:", content[1])

        if (do_compute_relevancies): # compute relevancies
            gene_exp = [float(content[i]) for i in range(sample_begin, sample_end)]
            F_statistic_value = F_statistic(classes_names, class_labels_ordered, gene_exp, samples_per_class=class_numbers) # warning: class names are case-sensitive
            relevancy_list.append(F_statistic_value)
            if (F_statistic_value > largest_relevancy):
                largest_relevancy = F_statistic_value
                largest_relevancy_index = g
                #print(largest_relevancy)

            i = 0
            add_to_list = True
            for i in range(len(relevancy_indices_list_sorted)): # sorting the relevancies
                if (i >= n_genes_to_select and get_all_relevancies == False):
                    add_to_list = False
                    break
                if (F_statistic_value > relevancy_list[relevancy_indices_list_sorted[i]]):
                    i = i - 1
                    break
            if (add_to_list):
                relevancy_indices_list_sorted.insert(i + 1, (g - (dtable_begin + 2))) # insert relative index
    write_genes.close()
    
    if (get_all_relevancies and do_compute_relevancies): # outputing relevancies by gene, in sorted order
        relevancies_writefile = open((work_dir + "\\" + samp + "_gene_relevancies.txt"), 'w')
        relevancies_writefile.write("Index (in-file),Gene,Gene ID,F-statistic")
        for r in range(len(relevancy_indices_list_sorted)):
            index = relevancy_indices_list_sorted[r] # index relative from dtable_begin
            index_in_file = index + (dtable_begin + 2) # in-file index (line-by-line)
            gname = file_parsed[index_in_file][1]
            gid = file_parsed[index_in_file][0] # gene identifier (some gene symbols have more than one identifier --> alternative splicing?)
            relevancy = relevancy_list[index]
            relevancies_writefile.write(("\n" + str(index_in_file) + "," + gname + "," + gid + "," + str(relevancy)))
        relevancies_writefile.close()
        file_relevancies = open((work_dir + "\\" + samp + "_gene_relevancies.txt")) # improve efficiency; change this
    
    if (do_compute_relevancies == False or do_compute_relevancies == True): # load the relevancies file if so configured; right now we always do this; could be improved
        print("Loading relevancies")
        relevancies_indices_list_sorted = []
        relevancy_list = [-1 for _ in range(num_genes)]
        for line_rel in file_relevancies:
            if (("Index (in-file),Gene,Gene ID,F-statistic" not in line_rel) and line_rel != "\n" and line_rel != ""):
                if (line_rel[-1] == "\n"):
                    pline_rel = line_rel[0:-1]
                else:
                    pline_rel = line_rel
                rel_split = pline_rel.split(",")
                index = int(rel_split[0]) # in-file index
                val = float(rel_split[3])
                relevancies_indices_list_sorted.append(index) # in-file index
                relevancy_list[index - (dtable_begin + 2)] = val # relative index
        file_relevancies.close()

    # now for the actual mRMR selection
    mRMR_genes_selected = [] # (index, name, relevancy); redundancy and mRMR score follow later
    mrmr_mode = configuration["mrmr_mode"]
    pearson_sum_mask = [0 for _ in range(num_genes)] # keep Pearson CC sums with so-far known set to remove redundant calculations
    if (mrmr_mode not in ["FCD", "FCQ"]):
        print("Invalid mRMR mode in configuration:", mrmr_mode)

    printer_frequency = 10**(int(math.log(num_genes, 10)) - 4) # it's so slow that we don't need this actually
    printer_frequency = [1, printer_frequency][printer_frequency > 1]
    rel_weight = configuration["mrmr_rel_weight"]
    red_weight = configuration["mrmr_red_weight"]
    for i in range(0, n_genes_to_select):
        if (i % printer_frequency == 0): # make sure not to spam too much
            print("mRMR selecting gene number:", i)
            
        if (i == 0): # first gene: just check relevancies
            mRMR_score_best_so_far = "N/A"
            gene_index_to_select = relevancies_indices_list_sorted[0] # relative index
            gene_index_to_select_in_file = relevancies_indices_list_sorted[0] + (dtable_begin + 2) # in-file index
            gene_tuple = (gene_index_to_select_in_file, file_parsed[gene_index_to_select_in_file][1], file_parsed[gene_index_to_select_in_file][0], relevancy_list[gene_index_to_select]) # index (in-file) | gname | g_id | relevancy
        else:
            mRMR_score_best_so_far = False
            gene_index_best_so_far = -1
            most_recent_newcomer = mRMR_genes_selected[i - 1][0] # in-file index
            content_mrn = file_parsed[most_recent_newcomer]
            gene_exp_mrn = [float(content_mrn[k]) for k in range(sample_begin, sample_end)]
            for j in range(num_genes):
                if (j % 1000 == 0): # remove this to remove per-gene progress output
                    print(j)
                skip = False
                for selg in mRMR_genes_selected: # must not select a gene already in the set
                    if (selg[0] == (j + (dtable_begin + 2))):
                        skip = True
                        
                if (skip == False):
                    gindex = j + (dtable_begin + 2)
                    content = file_parsed[gindex]
                    gene_exp = [float(content[k]) for k in range(sample_begin, sample_end)]
                    pcc = Pearson_correlation(gene_exp_mrn, gene_exp) # get the redundancy: PCC
                    reinflate_pearson = pearson_sum_mask[j] * (i - 1) # get previous sum of PCC's
                    pearson_sum_mask[j] = (reinflate_pearson + abs(pcc)) / (i) # reduce/deflate sum of PCC's again: we now have i elements in the set
                    curr_relevancy = relevancy_list[j] # get the relevancy: F-statistic
                    if (mrmr_mode == "FCD"): # get the mRMR score
                        mRMR_score = (rel_weight * curr_relevancy) - (red_weight * pearson_sum_mask[j])
                    elif (mrmr_mode == "FCQ"):
                        mRMR_score = (rel_weight * curr_relevancy) / (red_weight * pearson_sum_mask[j])

                    if (mRMR_score_best_so_far == False):
                        mRMR_score_best_so_far = mRMR_score
                        gene_index_best_so_far = j
                    else:
                        if (mRMR_score > mRMR_score_best_so_far):
                            mRMR_score_best_so_far = mRMR_score
                            gene_index_best_so_far = j # relative index
            gene_index_to_select = gene_index_best_so_far + (dtable_begin + 2) # in-file index
            gene_tuple = (gene_index_to_select, file_parsed[gene_index_to_select][1], file_parsed[gene_index_to_select][0], relevancy_list[gene_index_best_so_far]) # index (in-file) | gname | g_id | relevancy

        if (i % printer_frequency == 0): # make sure not to spam too much
            print("mRMR selected:", gene_tuple[1], "( ID:", gene_tuple[2], ")", "with score:", mRMR_score_best_so_far)
        mRMR_genes_selected.append(gene_tuple)

    mRMR_output_file = open((paste_dir + "\\" + samp + "_mRMR_selections_" + str(n_genes_to_select) + "_" + mrmr_mode + ".txt"), 'w')
    mRMR_output_file.write(("Index,Gene,Gene ID,Relevancy,Redundancy,mRMR score")) # gene identifier (some gene symbols have more than one identifier --> alternative splicing?)
    for tup in mRMR_genes_selected:
        pcc_total = 0
        for tup2 in mRMR_genes_selected: # we need to re-compute the PCC's because the set has changed
            if (tup[0] != tup2[0]): # in-file indices
                gene_exp1 = [float(file_parsed[tup[0]][k]) for k in range(sample_begin, sample_end)]
                gene_exp2 = [float(file_parsed[tup2[0]][k]) for k in range(sample_begin, sample_end)]
                pcc = Pearson_correlation(gene_exp1, gene_exp2) # get the redundancy: PCC
                pcc_total += abs(pcc)
        pcc_total = pcc_total / n_genes_to_select
        if (mrmr_mode == "FCD"): # get the mRMR score
                mRMR_score = (rel_weight * tup[3]) - (red_weight * pcc_total)
        elif (mrmr_mode == "FCQ"):
                mRMR_score = (rel_weight * tup[3]) / (red_weight * pcc_total)
                
        mRMR_output_file.write(("\n" + str(tup[0]) + "," + tup[1] + "," + tup[2] + "," + str(tup[3])))
        mRMR_output_file.write(("," + str(pcc_total) + "," + str(mRMR_score)))

    mRMR_output_file.close()
