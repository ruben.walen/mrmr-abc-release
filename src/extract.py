import os

import config

configuration = config.getConfig()
data_dir = os.getcwd() + "\\" + configuration["data_directory"]
paste_dir = os.getcwd() + "\\" + configuration["output_directory"]
work_dir = os.getcwd() + "\\" + configuration["working_directory"]
samples = configuration["sample_names"]

try:
    os.mkdir(paste_dir)
except FileExistsError:
    print("output directory already exists")
try:
    os.mkdir(work_dir)
    #os.chdir(work_dir)
except FileExistsError:
    print("work directory already exists")

for samp in samples:
    print(samp)
    directory = data_dir + "\\" + samp
    filename = os.listdir(directory)[0]
    file_handle = open((directory + "\\" + filename))
    file_parsed = []
    line_classification = []
    for line in file_handle: # handling SOFT data
        culled_line = line[0:-1]
        entry_type = culled_line.split(" = ")
        if (len(entry_type) > 1):
            line_classification.append(entry_type[0]) # metadata separated by "key = value"
        elif (culled_line[0] == "!"):
            line_classification.append(culled_line) # !dataset_table_begin
        else:
            line_classification.append("data") # body data
        file_parsed.append(culled_line.split("\t")) # the line itself
    file_handle.close()

    # extract metadata
    sample_types = {}
    nsamp_cancer = 0
    nsamp_normal = 0
    nsamp_unknown = 0
    classes_names = configuration["classes"]
    classes_names_ids = configuration["class_ids"]
    if (len(classes_names) > 2):
        print("Multiclass classification is not supported yet")
        exit
    
    for i in range(len(line_classification)):
        if (line_classification[i] == "^DATASET"): # start of metadata
            index_dataset_start = i
            
        if ("GSM" in line_classification[i]): # sample name
            samp_name = line_classification[i]
            line = file_parsed[i][0]
            ### TODO: for different datasets ###
            if (classes_names_ids[1] in line):
                sample_types[samp_name] = classes_names[1]
                nsamp_cancer += 1 # this refers to the typical classes; it is not necessarily a cancer-not cancer distinction
            elif (classes_names_ids[0] in line):
                sample_types[samp_name] = classes_names[0]
                nsamp_normal += 1
            else:
                sample_types[samp_name] = "Unknown"
                nsamp_unknown += 1
                
        if (line_classification[i] == "!dataset_table_begin"): # start of expression profiles
            index_datatable_begin = i

        if (line_classification[i] == "!dataset_table_end"):
            index_datatable_end = i

    print("Found metadata: metadata start | table start | table end:", index_dataset_start, index_datatable_begin, index_datatable_end)
    print("Samples:", classes_names[1], ":", nsamp_cancer, classes_names[0], ":", nsamp_normal, "Unknown:", nsamp_unknown)
    header = file_parsed[index_datatable_begin+1]

    file_output = open((work_dir + "\\" + samp + "_meta.txt"), 'w')
    file_output.write("METADATA.DATASETNAME = " + samp + "\n")
    file_output.write("FILENAME = " + filename + "\n" + "SAMPLE_TYPES = ")
    for s in range(len(sample_types.keys())):
        k = list(sample_types.keys())[s]
        file_output.write("\"" + k + "\"")
        file_output.write(" : " + "\"" + sample_types[k] + "\"")
        if (s < (len(sample_types.keys()) - 1)):
            file_output.write(",")
    file_output.write("\n" + "NSAMPLES = " + str(nsamp_cancer) + "," + str(nsamp_normal) + "," + str(nsamp_unknown) + "\n")
    file_output.write("DATA_INDICES = " + str(index_dataset_start) + "," + str(index_datatable_begin) + "," + str(index_datatable_end) + "\n" + "HEADER = ")
    for h in range(len(header)):
        file_output.write("\"" + header[h] + "\"")
        if (h < (len(header) - 1)):
            file_output.write(",")
    file_output.close()
