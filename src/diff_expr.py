import os
import math
import random
import numpy as np
import scipy.stats
import statistics

import config

## compute differential expression of genes in the dataset (binary classes) ##
## assume independent t-test can be used ##
## requires normalised micro-array data ##

configuration = config.getConfig()
data_dir = os.getcwd() + "\\" + configuration["data_directory"]
paste_dir = os.getcwd() + "\\" + configuration["output_directory"]
work_dir = os.getcwd() + "\\" + configuration["working_directory"]
samples = configuration["sample_names"]

seed = configuration["random_seed"]
if (seed != "random"):
    random.seed(int(seed))

for samp in samples:
    # retrieve metadata
    print(samp)
    try:
        metadata_file = open((work_dir + "\\" + samp + "_meta.txt"))
    except FileNotFoundError:
        print("Could not find metadata for:", samp, "\nFirst run extract.py with the correct configuration")
        exit
    metadata = config.processMetadata(config.readMetadata(metadata_file))
    metadata_file.close()

    # get the data
    directory = data_dir + "\\" + samp
    filename = os.listdir(directory)[0]
    file_handle = open((directory + "\\" + filename))
    file_parsed = []
    line_classification = []
    for line in file_handle: # handling SOFT data
        culled_line = line[0:-1]
        entry_type = culled_line.split(" = ")
        if (len(entry_type) > 1):
            line_classification.append(entry_type[0]) # metadata separated by "key = value"
        elif (culled_line[0] == "!"):
            line_classification.append(culled_line) # !dataset_table_begin
        else:
            line_classification.append("data") # body data
        file_parsed.append(culled_line.split("\t")) # the line itself
    file_handle.close()

    # get some indices, retrieve the classes
    sample_begin = 2
    class_labels_ordered = []
    classes_names = configuration["classes"]
    classes_names_ids = configuration["class_ids"]
    for h in range(sample_begin, (len(metadata["HEADER"]) - sample_begin)):
        if ("GSM" not in metadata["HEADER"][h]):
            sample_end = h
            break
        else:
            class_labels_ordered.append(metadata["SAMPLE_TYPES"]["#" + metadata["HEADER"][h]])
            if (class_labels_ordered[-1] == "Unknown"):
                print("Unknown labelled samples detected. Please correct the data.")
                exit

    # samples
    sample_begin = 2
    for h in range(sample_begin, (len(metadata["HEADER"]) - sample_begin)):
        if ("GSM" not in metadata["HEADER"][h]):
            sample_end = h
            break

    # compute differential expression
    print("Begin differential expression computation")
    dtable_begin = metadata["DATA_INDICES"]["DATATABLE_BEGIN"]
    num_genes = metadata["DATA_INDICES"]["DATATABLE_END"] - (dtable_begin + 2)
    write_diff = open((paste_dir + "\\" + samp + "_diff_exp.txt"), 'w') # write a file containing: gene | gene ID | fold exp | ttest T-value | ttest p-value
    write_diff.write("Gene symbol,Gene identifier,log10 fold expression,T score,p-value (T-test)")
    printer_frequency = 10**(int(math.log(num_genes, 10)) - 1)
    printer_frequency = [1, printer_frequency][printer_frequency > 1]

    print("Number of genes:", num_genes)
    for g in range(dtable_begin + 2, metadata["DATA_INDICES"]["DATATABLE_END"]):
        if ((g - (dtable_begin + 2)) % printer_frequency == 0): # make sure not to spam too much
            print("Differential expression: gene number:", (g - (dtable_begin + 2)))
            
        content = file_parsed[g]
        gene_exp = [float(content[i]) for i in range(sample_begin, sample_end)]
        gene_exp_c1 = []
        gene_exp_c2 = []
        for i in range(len(gene_exp)):
            if (class_labels_ordered[i] == classes_names[0]): # typical: 'Normal'
                gene_exp_c1.append(gene_exp[i])
            elif (class_labels_ordered[i] == classes_names[1]):
                gene_exp_c2.append(gene_exp[i])

        fold_exp = math.log((statistics.mean(gene_exp_c2) / statistics.mean(gene_exp_c1)), 10) # log10 fold expression
        ttest = scipy.stats.ttest_ind(gene_exp_c1, gene_exp_c2, equal_var=False) # assume unequal variances

        write_diff.write(('\n' + content[1] + "," + content[0] + "," + str(fold_exp) + "," + str(ttest[0])))
        write_diff.write(("," + str(ttest[1])))

    write_diff.close()
    print("Done writing differential expression")
