config_name = "configuration.txt"

def getConfig():
    try:
        config = open(config_name)
    except FileNotFoundError:
        print("Could not find configuration")
        return False

    fields = ["output_directory", "data_directory", "working_directory",
              "sample_names", "mrmr_number_of_genes", "mrmr_output_all_relevancies",
              "mrmr_use_relevancies_file", "mrmr_mode", "mrmr_rel_weight",
              "mrmr_red_weight", "abc_employee_bees", "abc_onlooker_bees",
              "abc_abandon_source_after", "abc_nflips_probability_control",
              "abc_genes_to_select", "loocv_sets", "abc_svm_accuracy_weight",
              "abc_svm_precision_weight", "abc_svm_recall_weight", "random_seed",
              "abc_max_iterations", "abc_num_repeats", "classes", "class_ids",
              "select_random_genes", "loocv_random_sets", "load_diff_expr_list",
              "use_ga_instead_of_abc", "ga_mu_population", "ga_p_crossover",
              "ga_nflips_probability_control", "ga_selection_alpha_weight",
              "do_per_run_loocv_classification"]

    config_parsed = {}
    copy_fields = fields
    for line in config:
        if (line[-1] == "\n"):
            pline = line[0:-1]
        else:
            pline = line
            
        elems = pline.split(" = ")
        rem_flag = False
        if (elems[0] == ""):
            pass
        elif (elems[0] == "\n"):
            pass
        elif (elems[0][0] == "#"): # comment
            pass
        elif (elems[0] not in fields):
            print("Invalid field in configuration:", elems[0])
            return False
        elif (elems[0] == "sample_names" or
              elems[0] == "classes" or
              elems[0] == "class_ids"):
            rem_flag = True
            data_names = elems[1][1:-1]
            data_names = data_names.split(",")
            config_parsed[elems[0]] = data_names
        elif (elems[0] == "mrmr_number_of_genes" or
              elems[0] == "abc_employee_bees" or
              elems[0] == "abc_abandon_source_after" or
              elems[0] == "abc_onlooker_bees" or
              elems[0] == "loocv_sets" or
              elems[0] == "abc_max_iterations" or
              elems[0] == "abc_num_repeats" or
              elems[0] == "loocv_random_sets" or
              elems[0] == "ga_mu_population"): # int
            rem_flag = True
            number = int(elems[1])
            config_parsed[elems[0]] = number
        elif (elems[0] == "abc_genes_to_select"): # int
            rem_flag = True
            number = int(elems[1])
            config_parsed[elems[0]] = number
            if (number > config_parsed["mrmr_number_of_genes"]):
                print("Number of genes to select in ABC is larger than selected from mRMR!")
        elif (elems[0] == "mrmr_rel_weight" or # float
              elems[0] == "mrmr_red_weight" or
              elems[0] =="abc_svm_accuracy_weight" or
              elems[0] =="abc_svm_precision_weight" or
              elems[0] =="abc_svm_recall_weight"):
            rem_flag = True
            number = float(elems[1])
            config_parsed[elems[0]] = number
        elif (elems[0] == "abc_nflips_probability_control" or
              elems[0] == "ga_p_crossover" or
              elems[0] == "ga_nflips_probability_control" or
              elems[0] == "ga_selection_alpha_weight"): # float, fractional possible
            rem_flag = True
            l_split = elems[1].split("/")
            if (len(l_split) == 2):
                number = float(l_split[0]) / float(l_split[1]) # i.e. '1/4'
            else:
                number = float(elems[1])
            config_parsed[elems[0]] = number
        elif (elems[0] == "mrmr_output_all_relevancies" or # boolean
              elems[0] == "mrmr_use_relevancies_file" or
              elems[0] == "select_random_genes" or
              elems[0] == "load_diff_expr_list" or
              elems[0] == "use_ga_instead_of_abc" or
              elems[0] == "do_per_run_loocv_classification"):
            rem_flag = True
            boolval = [False, True][elems[1] == "True"]
            config_parsed[elems[0]] = boolval
        else:
            rem_flag = True
            config_parsed[elems[0]] = elems[1]

        if (rem_flag):
            copy_fields.remove(elems[0])

    if len(copy_fields) != 0:
        print("Config warning: incomplete keys; missing:", copy_fields)

    return config_parsed

def readMetadata(file):
    metadata = {}
    for line in file:
        culled_line = line[0:-1]
        sep = culled_line.split(" = ")
        if (len(sep) != 2):
            print("Error in metadata line:", culled_line)
            exit
        metadata[sep[0]] = sep[1]
    return metadata

def processMetadata(metadata):
    metadata_parsed = {}
    for k in metadata.keys():
        content = metadata[k]
        if (content != ''):
            if (k == "SAMPLE_TYPES"):
                samp_list = content.split(",")
                samp_dict = {}
                for e in samp_list:
                    e_split = e.split(" : ")
                    samp_dict[e_split[0][1:-1]] = e_split[1][1:-1]
                metadata_parsed["SAMPLE_TYPES"] = samp_dict
            elif (k == "NSAMPLES"):
                nsamp_list = content.split(",")
                nsamp_dict = {}
                nsamp_dict["CANCER"] = int(nsamp_list[0])
                nsamp_dict["NORMAL"] = int(nsamp_list[1])
                nsamp_dict["UNKNOWN"] = int(nsamp_list[2])
                metadata_parsed["NSAMPLES"] = nsamp_dict
            elif (k == "DATA_INDICES"):
                ndata_list = content.split(",")
                ndata_dict = {}
                ndata_dict["DATASET_START"] = int(ndata_list[0])
                ndata_dict["DATATABLE_BEGIN"] = int(ndata_list[1])
                ndata_dict["DATATABLE_END"] = int(ndata_list[2])
                metadata_parsed["DATA_INDICES"] = ndata_dict
            elif (k == "HEADER"):
                header_list = content.split(",")
                header_list2 = []
                for h in header_list:
                    header_list2.append(h[1:-1])
                metadata_parsed["HEADER"] = header_list2
            else:
                metadata_parsed[k] = content
    return metadata_parsed
