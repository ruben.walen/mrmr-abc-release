import os
import math
import random
import numpy as np
from functools import partial
import datetime
from scipy import stats

import config
import machines

configuration = config.getConfig()
data_dir = os.getcwd() + "\\" + configuration["data_directory"]
paste_dir = os.getcwd() + "\\" + configuration["output_directory"]
work_dir = os.getcwd() + "\\" + configuration["working_directory"]
samples = configuration["sample_names"]

seed = configuration["random_seed"]
if (seed != "random"):
    random.seed(int(seed))

for samp in samples:
    # retrieve metadata
    print(samp)
    try:
        metadata_file = open((work_dir + "\\" + samp + "_meta.txt"))
    except FileNotFoundError:
        print("Could not find metadata for:", samp, "\nFirst run extract.py with the correct configuration")
        exit
    metadata = config.processMetadata(config.readMetadata(metadata_file))
    metadata_file.close()

    # get the data
    directory = data_dir + "\\" + samp
    filename = os.listdir(directory)[0]
    file_handle = open((directory + "\\" + filename))
    file_parsed = []
    line_classification = []
    for line in file_handle: # handling SOFT data
        culled_line = line[0:-1]
        entry_type = culled_line.split(" = ")
        if (len(entry_type) > 1):
            line_classification.append(entry_type[0]) # metadata separated by "key = value"
        elif (culled_line[0] == "!"):
            line_classification.append(culled_line) # !dataset_table_begin
        else:
            line_classification.append("data") # body data
        file_parsed.append(culled_line.split("\t")) # the line itself
    file_handle.close()

    # get some indices, retrieve the classes
    sample_begin = 2
    class_labels_ordered = []
    for h in range(sample_begin, (len(metadata["HEADER"]) - sample_begin)):
        if ("GSM" not in metadata["HEADER"][h]):
            sample_end = h
            break
        else:
            class_labels_ordered.append(metadata["SAMPLE_TYPES"]["#" + metadata["HEADER"][h]])
            if (class_labels_ordered[-1] == "Unknown"):
                print("Unknown labelled samples detected. Please correct the data.")
                exit

    load_diff_expr_list = configuration["load_diff_expr_list"]
    # retrieve the mRMR selected genes. Attempts to find the file as described from the configuration, i.e. 20 genes, FCD selection
    try:
        if (load_diff_expr_list == False):
            filename_mrmr = (samp + "_mRMR" + "_selections_" + str(configuration["mrmr_number_of_genes"]) + "_" + configuration["mrmr_mode"] + ".txt")
            file_mrmr = open((paste_dir + "\\" + filename_mrmr))
        elif (load_diff_expr_list == True): # use list of differentially expressed genes as feature selection
            filename_mrmr = (paste_dir + "\\" + samp + "_diff_exp.txt")
            file_mrmr = open(filename_mrmr)
    except FileNotFoundError:
        print("Could not find mRMR feature file. Make sure to run the mRMR feature selection (with the configured settings) first!", filename_mrmr)
        exit

    # control gene selection: if True, use the mRMR feature-selected genes (must run mRMR.py first). If False, select random genes --> test the ABC algorithm.
    load_mRMR_genes = [True, False][configuration["select_random_genes"]]
    dtable_begin = metadata["DATA_INDICES"]["DATATABLE_BEGIN"]
    num_genes = metadata["DATA_INDICES"]["DATATABLE_END"] - (dtable_begin + 2)

    if (load_mRMR_genes):
        if (load_diff_expr_list == False):
            print("Gene pre-selection: loading mRMR features")
            mRMR_list = []
            for line_mrmr in file_mrmr:
                if (("Index,Gene,Gene ID,Relevancy,Redundancy,mRMR score" not in line_mrmr) and line_mrmr != "\n" and line_mrmr != ""):
                    if (line_mrmr[-1] == "\n"):
                        pline_mrmr = line_mrmr[0:-1]
                    else:
                        pline_mrmr = line_mrmr
                    mrmr_split = pline_mrmr.split(",")
                    index = int(mrmr_split[0])
                    gname = mrmr_split[1]
                    gid = mrmr_split[2]
                    rel_val = float(mrmr_split[3])
                    red_val = float(mrmr_split[4])
                    mrmr_score_val = float(mrmr_split[5])
                    mRMR_list.append((index, gname, gid, rel_val, red_val, mrmr_score_val))
            file_mrmr.close()
            
        elif (load_diff_expr_list == True): # use list of differentially expressed genes as feature selection
            print("Gene pre-selection: loading differential expression features")
            mRMR_list = [] # in this case, these are now differential expression genes
            line_num = 0
            for line_diff in file_mrmr:
                if (("Gene symbol,Gene identifier" not in line_diff) and line_diff != "\n" and line_diff != ""):
                    if (line_diff[-1] == "\n"):
                        pline_diff = line_diff[0:-1]
                    else:
                        pline_diff = line_diff
                    diff_split = pline_diff.split(",")
                    index = line_num + (dtable_begin + 2)
                    gname = diff_split[0]
                    gid = diff_split[1]
                    absolute_diff_exp = abs(float(diff_split[2])) # absolute fold-expression
                    mRMR_list.append((index, gname, gid, absolute_diff_exp))
                    line_num += 1
            file_mrmr.close()
    
    elif (load_mRMR_genes == False):
        print("Gene pre-selection: selecting random genes (non-mRMR)")
        mRMR_list = []
        for i in range(configuration["mrmr_number_of_genes"]):
            cont = False
            while (cont == False):
                select_at_random = int(random.random() * num_genes)
                g = select_at_random + (dtable_begin + 2)
                content = file_parsed[g]
                name = content[1]
                gid = content[0]
                cont = True
                for m in mRMR_list:
                    if (m[0] == g): # it's already in the list
                        cont = False
                        break # break out of for
                if (cont == True):
                    mRMR_list.append([g, name, gid]) # tuple now holds just one index: the in-file gene index (randomly selected)
                    # break out of while with cont statement

    if (load_diff_expr_list == True): # must sort this list first TODO: pre-sort differential expression file; skip this step
        print("Sorting differential expression genes list")
        mRMR_list.sort(key = (lambda val: val[3]), reverse = True)
        num_genes_selected = configuration["mrmr_number_of_genes"] # take e.g. 100 genes
        mRMR_list = mRMR_list[0:num_genes_selected]

    # retrieve gene expression data array for those mRMR genes
    print("Constructing gene expression table")
    class_numbers = (metadata["NSAMPLES"]["CANCER"], metadata["NSAMPLES"]["NORMAL"]) # this refers to the typical classes; it is not necessarily a cancer-not cancer distinction
    num_genes_selected = len(mRMR_list)
    classes_names = configuration["classes"]
    classes_names_ids = configuration["class_ids"]

    gene_exp_array = np.zeros((num_genes_selected, class_numbers[0] + class_numbers[1]))
    i = 0
    for gene in mRMR_list:
        g = gene[0] # index (in-file)
        content = file_parsed[g]
        gene_exp = np.array([float(content[j]) for j in range(sample_begin, sample_end)])
        gene_exp_array[i, :] = gene_exp
        i += 1

    # LOOCV sets
    num_sets = configuration["loocv_sets"]
    loocv_indices = machines.getIndexSetsLOOCV(classes_names, {classes_names[1] : class_numbers[0], classes_names[0] : class_numbers[1]}, # 0 -> 1, 1 -> 0 should be correct here, see extract.py
                                               class_labels_ordered, num_sets, return_sample_distr_info=False)

    # do the bee stuff: ABC with SVM parameters as objective function
    n_emp_bees = configuration["abc_employee_bees"]
    n_onl_bees = configuration["abc_onlooker_bees"]
    abc_n_genes_to_select = configuration["abc_genes_to_select"]
    n_cycles_to_abandon = configuration["abc_abandon_source_after"]
    p_flip = configuration["abc_nflips_probability_control"]
    vec_counts = (num_genes_selected - abc_n_genes_to_select, abc_n_genes_to_select) # 0s and 1s: how many genes are we allowed to select?
    vec_length = num_genes_selected

    # we can also do genetic algorithm feature selection; control parameters here
    do_GA_instead_of_ABC = configuration["use_ga_instead_of_abc"]
    GA_mu_population = configuration["ga_mu_population"]
    GA_p_crossover = configuration["ga_p_crossover"]
    GA_mutation_control = configuration["ga_nflips_probability_control"]
    GA_selection_alpha_weight = configuration["ga_selection_alpha_weight"]

    # objective function - classification result weights
    of_w_accuracy = configuration["abc_svm_accuracy_weight"]
    of_w_precision = configuration["abc_svm_precision_weight"]
    of_w_recall = configuration["abc_svm_recall_weight"]

    max_iter = configuration["abc_max_iterations"]
    n_repeats = configuration["abc_num_repeats"]
    
    labels_numeric = np.array([(0, 1)[class_labels_ordered[l] == classes_names[1]] for l in range(class_numbers[0] + class_numbers[1])]) # numeric label conversion

    # evaluate using k-folded SVM for every run, not just at the end as a combination
    do_per_run_loocv_evaluation = configuration["do_per_run_loocv_classification"] # from config
    if (do_per_run_loocv_evaluation):
        print("Evaluating LOOCV accuracy each run. Iterations may take longer.")
        per_run_train_scores = [[0, 0, 0] for _ in range(n_repeats)]
        per_run_test_scores = [[0, 0, 0] for _ in range(n_repeats)]

        test_set_indices_perrun = [None for _ in range(num_sets)]
        train_set_indices_perrun = [None for _ in range(num_sets)]
        gene_exp_train_perrun = [None for _ in range(num_sets)]
        gene_exp_test_perrun = [None for _ in range(num_sets)]
        labels_train_perrun = [None for _ in range(num_sets)]
        labels_test_perrun = [None for _ in range(num_sets)]
        for k in range(num_sets): # K-fold
            test_set_indices_perrun[k] = loocv_indices[k]
            train_set_indices_perrun[k] = []
            for l in range(num_sets):
                if (l != k):
                    train_set_indices_perrun[k].extend(loocv_indices[l])

            gene_exp_train_perrun[k] = gene_exp_array[:, train_set_indices_perrun[k]] # gene expression split arrays
            gene_exp_test_perrun[k] = gene_exp_array[:, test_set_indices_perrun[k]]
            labels_train_perrun[k] = np.array([(0, 1)[class_labels_ordered[l] == classes_names[1]] for l in train_set_indices_perrun[k]]) # numeric label conversion
            labels_test_perrun[k] = np.array([(0, 1)[class_labels_ordered[l] == classes_names[1]] for l in test_set_indices_perrun[k]])
    
    # evaluator: objective function; passed as partial function
    evaluator = partial(machines.wrapperSVM_DOF, gene_exp_train=gene_exp_array, y_train=labels_numeric, gene_exp_test=gene_exp_array, y_test=labels_numeric,
                        w_accuracy=of_w_accuracy, w_precision=of_w_precision, w_recall=of_w_recall, kernel='rbf')

    # bee stuff starts here, I promise
    print_fitnesses = False # debug
    gene_occurence_stats = [(0, 0) for _ in range(num_genes_selected)] # (occurences, average fitness) for every selectable gene
    best_overall_solution = None # best overall solution from all runs
    best_overall_solution_score = -1
    for p in range(0, n_repeats):
        print("Running", ["ABC,", "GA,"][do_GA_instead_of_ABC], "iteration", p)
        if (do_GA_instead_of_ABC == False):
            bees = machines.FSArtificialBeeColony(n_emp_bees, n_onl_bees, n_cycles_to_abandon, vec_length, vec_counts, evaluator, p_flip) # the ABC
        elif (do_GA_instead_of_ABC == True):
            bees = machines.FSGeneticAlgorithm(GA_mu_population, GA_selection_alpha_weight, GA_p_crossover, 1, vec_length, vec_counts, evaluator, GA_mutation_control) # the GA, (still called bees :) )
            # crossover_npoints is broken currently, just set it to something (1 here)

        # let the bees converge, use the whole dataset; real classifier checking with LOOCV will come later
        for m in range(0, max_iter):
            bees.step() # callable for both ABC and GA
            if (print_fitnesses): # debug
                print(bees.best_so_far_fitness)

        if (do_GA_instead_of_ABC == False): # ABC
            solution_found = bees.emp_solutions[bees.best_so_far_solution_emp_index]
            solution_found_score = bees.best_so_far_fitness
        elif (do_GA_instead_of_ABC == True): # GA
            solution_found = bees.solutions[bees.best_so_far_solution_index]
            solution_found_score = bees.best_so_far_fitness

        for m in range(np.shape(solution_found)[0]): # count occurences of each gene in final best solutions
            if (solution_found[m] == 1):
                prev_stats = gene_occurence_stats[m]
                deflate_avg = prev_stats[1] * prev_stats[0]
                gene_occurence_stats[m] = (prev_stats[0] + 1, (deflate_avg + solution_found_score) / (prev_stats[0] + 1))

        if (solution_found_score > best_overall_solution_score): # update best found solution
            best_overall_solution = solution_found
            best_overall_solution_score = solution_found_score

        # k-folded secondary SVM, if enabled
        if (do_per_run_loocv_evaluation):
                indices = np.where(solution_found == 1)[0]

                for k in range(num_sets): # K-fold
                    gene_exp_train = gene_exp_train_perrun[k] # gene expression split arrays
                    gene_exp_test = gene_exp_test_perrun[k]
                    labels_train = labels_train_perrun[k] # numeric label conversion
                    labels_test = labels_test_perrun[k]

                    sub_gene_exp_array_train = gene_exp_train[indices, :].T # need to invert the axes? pick gene subset
                    sub_gene_exp_array_test = gene_exp_test[indices, :].T

                    score = machines.defaultPredictSVM(sub_gene_exp_array_train, labels_train, sub_gene_exp_array_test, labels_test, also_predict_train=True) # call, fit, predict SVM

                    for q in range(3): # accuracy, precision, recall per run
                        per_run_train_scores[p][q] = score["train"][q]
                        per_run_test_scores[p][q] = score["test"][q]

    print("Done with ABC step")

    # construct selection out of best genes found from all ABC iterations
    best_selection_vector = np.zeros((vec_length))
    preselection_ranked_selection_vector = np.zeros((vec_length))
    best_selection_ranking = []
    best_selected_remainder = vec_length
    for g in range(len(gene_occurence_stats)):
        gene = gene_occurence_stats[g]
        g2 = 0
        skip = False
        for g2 in range(len(best_selection_ranking) - 1, -1, -1): # count backwards
            gene2 = gene_occurence_stats[best_selection_ranking[g2][0]]
            if (best_selection_ranking[g2][0] != g): # not already in the list
                comp_score1 = (gene[0] / n_repeats) + gene[1] # comparative score: (number of occurences / number of repeats) + average score
                comp_score2 = (gene2[0] / n_repeats) + gene2[1]
                #print("1", "ind", g, " | ", comp_score1, gene[0], gene[1]) # some debug statements if the sorter acts weird again
                #print("2", "ind", best_selection_ranking[g2][0], " | ", comp_score2, gene2[0], gene2[1])
                if (comp_score1 < comp_score2): # if the old element is better
                    #print("ins", g2 + 1)
                    best_selection_ranking.insert(g2 + 1, (g, gene[0], gene[1])) # index | occurences | average score
                        
                    if (len(best_selection_ranking) > num_genes_selected): # if the vector is now too long
                        best_selection_ranking = best_selection_ranking[0:-1] # remove the last
                    skip = True
                    break
            else:
                skip = True # do not insert again

        if ((g2 == 0) and (skip == False)): # last element in the list
            #print("zerins")
            best_selection_ranking.insert(0, (g, gene[0], gene[1])) # index | occurences | average score
                
            if (len(best_selection_ranking) > num_genes_selected): # if the vector is now too long
                    best_selection_ranking = best_selection_ranking[0:-1] # remove the last
            
        best_selected_remainder -= 1 # we don't actually need this?
    #print(best_selection_ranking)

    # make the average best solution vector
    for g in range(abc_n_genes_to_select):
        gene = best_selection_ranking[g]
        best_selection_vector[gene[0]] = 1

    # make the vector consisting of the highest-ranked genes from the pre-selection
    for g in range(abc_n_genes_to_select):
        preselection_ranked_selection_vector[g] = 1 # the rest are zeros

    # make random 'best' solution vectors to test against: random sampling from pre-selection set
    random_selection_vectors = np.zeros((configuration["loocv_random_sets"], vec_length))
    for rs in range(configuration["loocv_random_sets"]):
        set_current = []
        for g in range(abc_n_genes_to_select):
            cont = False
            while (cont == False):
                index_sel = int(random.random() * vec_length)
                if (index_sel not in set_current):
                    cont = True
            set_current.append(index_sel)
        current_sel_vector = [[0, 1][sel_index in set_current] for sel_index in range(vec_length)]
        random_selection_vectors[rs, :] = np.array(current_sel_vector)
    
    # check classification accuracy with LOOCV
    indices = np.where(best_selection_vector == 1)[0]
    indices_preselection = np.where(preselection_ranked_selection_vector == 1)[0]
    indices_overall_best = np.where(best_overall_solution == 1)[0]

    train_score_avg = [0, 0, 0]
    test_score_avg = [0, 0, 0]
    train_score_avg_preselect = [0, 0, 0]
    test_score_avg_preselect = [0, 0, 0]
    train_score_avg_random = [0, 0, 0]
    test_score_avg_random = [0, 0, 0]
    train_score_avg_overall_best = [0, 0, 0]
    test_score_avg_overall_best = [0, 0, 0]
    
    for k in range(num_sets): # K-fold
        print("Classification test: Running fold", k)
        test_set_indices = loocv_indices[k]
        train_set_indices = []
        for l in range(num_sets):
            if (l != k):
                train_set_indices.extend(loocv_indices[l])

        gene_exp_train = gene_exp_array[:, train_set_indices] # gene expression split arrays
        gene_exp_test = gene_exp_array[:, test_set_indices]
        labels_train = np.array([(0, 1)[class_labels_ordered[l] == classes_names[1]] for l in train_set_indices]) # numeric label conversion
        labels_test = np.array([(0, 1)[class_labels_ordered[l] == classes_names[1]] for l in test_set_indices])

        sub_gene_exp_array_train = gene_exp_train[indices, :].T # need to invert the axes? pick gene subset
        sub_gene_exp_array_test = gene_exp_test[indices, :].T

        sgea_train_preselection = gene_exp_train[indices_preselection, :].T # pre-selection ranked vector
        sgea_test_preselection = gene_exp_test[indices_preselection, :].T

        sgea_train_overall_best = gene_exp_train[indices_overall_best, :].T # overall best ABC vector
        sgea_test_overall_best = gene_exp_test[indices_overall_best, :].T
        
        score = machines.defaultPredictSVM(sub_gene_exp_array_train, labels_train, sub_gene_exp_array_test, labels_test, also_predict_train=True) # call, fit, predict SVM
        score_preselection = machines.defaultPredictSVM(sgea_train_preselection, labels_train, sgea_test_preselection, labels_test, also_predict_train=True)
        score_overall_best = machines.defaultPredictSVM(sgea_train_overall_best, labels_train, sgea_test_overall_best, labels_test, also_predict_train=True)
        
        for q in range(3): # average accuracy, precision, recall
            train_score_avg[q] += score["train"][q]
            test_score_avg[q] += score["test"][q]
            train_score_avg_preselect[q] += score_preselection["train"][q]
            test_score_avg_preselect[q] += score_preselection["test"][q]
            train_score_avg_overall_best[q] += score_overall_best["train"][q]
            test_score_avg_overall_best[q] += score_overall_best["test"][q]

        for rs in range(configuration["loocv_random_sets"]): # the random solution vector classification
            indices_random = np.where(random_selection_vectors[rs, :] == 1)[0]
            sub_gene_exp_array_train = gene_exp_train[indices_random, :].T # need to invert the axes? pick gene subset
            sub_gene_exp_array_test = gene_exp_test[indices_random, :].T

            score = machines.defaultPredictSVM(sub_gene_exp_array_train, labels_train, sub_gene_exp_array_test, labels_test, also_predict_train=True) # call, fit, predict SVM
            for q in range(3): # average accuracy, precision, recall
                train_score_avg_random[q] += score["train"][q]
                test_score_avg_random[q] += score["test"][q]

    for q in range(3): # divide by number of random sets
        train_score_avg_random[q] = train_score_avg_random[q] / configuration["loocv_random_sets"]
        test_score_avg_random[q] = test_score_avg_random[q] / configuration["loocv_random_sets"]

    for q in range(3): # divide by number of k-folds
        train_score_avg[q] = train_score_avg[q] / num_sets
        test_score_avg[q] = test_score_avg[q] / num_sets
        train_score_avg_preselect[q] = train_score_avg_preselect[q] / num_sets
        test_score_avg_preselect[q] = test_score_avg_preselect[q] / num_sets
        train_score_avg_random[q] = train_score_avg_random[q] / num_sets
        test_score_avg_random[q] = test_score_avg_random[q] / num_sets
        train_score_avg_overall_best[q] = train_score_avg_overall_best[q] / num_sets
        test_score_avg_overall_best[q] = test_score_avg_overall_best[q] / num_sets
        
    print("Done with classification test")

    if (do_per_run_loocv_evaluation):
        print("Evaluating per-run LOOCV statistics")
        loocv_per_run_means_train = [sum([val[q] for val in per_run_train_scores]) / n_repeats for q in range(3)]
        loocv_per_run_means_test = [sum([val[q] for val in per_run_test_scores]) / n_repeats for q in range(3)]
        loocv_per_run_stdevs_train = [math.sqrt(sum([(val[q] - loocv_per_run_means_train[q])**2 for val in per_run_train_scores]) / (n_repeats - 1)) for q in range(3)] # Bessel corrected
        loocv_per_run_stdevs_test = [math.sqrt(sum([(val[q] - loocv_per_run_means_test[q])**2 for val in per_run_test_scores]) / (n_repeats - 1)) for q in range(3)]
        loocv_per_run_KS_train = [stats.kstest([val[q] for val in per_run_train_scores], 'norm') for q in range(3)] # Kolmogorov-Smirnov test of normal distribution of classifier metrics per run, alternative hypothesis = two-sided
        loocv_per_run_KS_test = [stats.kstest([val[q] for val in per_run_test_scores], 'norm') for q in range(3)] # returns (KS score, p-value)

        log_loocv_per_run_train = [[math.log(val[q]) for q in range(3)] for val in per_run_train_scores]
        log_loocv_per_run_test = [[math.log(val[q]) for q in range(3)] for val in per_run_test_scores]
        log_loocv_per_run_KS_train = [stats.kstest([val[q] for val in log_loocv_per_run_train], 'norm') for q in range(3)] # log-normal Kolmogorov-Smirnov
        log_loocv_per_run_KS_test = [stats.kstest([val[q] for val in log_loocv_per_run_test], 'norm') for q in range(3)]

    # writing data to results folder
    curr_time = str(datetime.datetime.now().time()).split(":")
    curr_time2 = str(datetime.datetime.now())
    random_id = "".join([("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f")[int(random.random() * 16)] for _ in range(4)]) # use unique ID to not overwrite files?
    abc_output_filename = (samp + "_" + "ABC_results_" + curr_time[0] + curr_time[1] + curr_time[2][0:2] + "_" + random_id + ".txt")
    abc_output_file = open((paste_dir + "\\" + abc_output_filename), 'w')
    abc_output_file.write(("Run results: " + curr_time2))
    print("Writing results to:", abc_output_filename)

    abc_output_file.write("\n\nSettings dump:\n")
    abc_output_file.write(("Class distinction: " + classes_names[0] + " | " + classes_names[1]))
    abc_output_file.write(("\nAll genes: " + str(num_genes)))
    abc_output_file.write(("\nGenes primary selection: " + str(num_genes_selected)))
    abc_output_file.write(("\nGenes primary selection: with mRMR/diff-exp (true) or at random (false): " + str(load_mRMR_genes)))
    abc_output_file.write(("\nIf previous true: genes primary selection: with mRMR (false) or differential expression (true): " + str(load_diff_expr_list)))
    abc_output_file.write(("\nTYPE OF SECOND SELECTION: ABC or GA: " + ["ABC", "GA"][do_GA_instead_of_ABC]))
    abc_output_file.write(("\nABC/GA genes to select: " + str(abc_n_genes_to_select)))
    abc_output_file.write(("\nABC employee bees: " + str(n_emp_bees)))
    abc_output_file.write(("\nABC onlooker bees: " + str(n_onl_bees)))
    abc_output_file.write(("\nABC abandon cycle count: " + str(n_cycles_to_abandon)))
    abc_output_file.write(("\nABC mutation flip probability control: " + str(p_flip)))
    abc_output_file.write(("\nGA population size: " + str(GA_mu_population)))
    abc_output_file.write(("\nGA crossover probability: " + str(GA_p_crossover)))
    abc_output_file.write(("\nGA prop. selection alpha weight: " + str(GA_selection_alpha_weight)))
    abc_output_file.write(("\nGA mutation flip probability control: " + str(GA_mutation_control)))
    abc_output_file.write(("\nABC/GA iterations: " + str(max_iter)))
    abc_output_file.write(("\nABC/GA reinitialisation/repeats: " + str(n_repeats)))
    abc_output_file.write(("\nABC/SVM score accuracy weight: " + str(of_w_accuracy)))
    abc_output_file.write(("\nABC/SVM score precision weight: " + str(of_w_precision)))
    abc_output_file.write(("\nABC/SVM score recall weight: " + str(of_w_recall)))
    abc_output_file.write(("\nSecond SVM number of folds: " + str(num_sets)))
    abc_output_file.write(("\nSecond SVM number of random sets to compare best selection: " + str(configuration["loocv_random_sets"])))

    abc_output_file.write("\n\nABC gene selection ranking:\n")
    abc_output_file.write("Gene symbol,Gene identifier,In-file index,ABC/SVM Occurences,ABC/SVM average score,Ranking comparison score")
    for gene in best_selection_ranking:
        index = gene[0]
        file_index = mRMR_list[index][0] # index (in-file)
        name = mRMR_list[index][1] # gene name
        identifier = mRMR_list[index][2] # gene identifier (some gene symbols have more than one identifier --> alternative splicing?)
        occ = gene[1]
        avgscore = gene[2]
        compscore = (occ / n_repeats) + avgscore
        abc_output_file.write(("\n" + str(name) + "," + str(identifier) + "," + str(file_index) + "," + str(occ) + "," + str(avgscore) + "," + str(compscore)))
    
    abc_output_file.write("\n\nSecond SVM run: best selection vs. random selection results:\n")
    abc_output_file.write("Set (average),Accuracy,Precision,Recall")
    abc_output_file.write(("\nTraining (second selection occurence-based best)," + str(train_score_avg[0]) + "," + str(train_score_avg[1]) + "," + str(train_score_avg[2])))
    abc_output_file.write(("\nTest (second selection occurence-based best)," + str(test_score_avg[0]) + "," + str(test_score_avg[1]) + "," + str(test_score_avg[2])))
    abc_output_file.write(("\nTraining (average random)," + str(train_score_avg_random[0]) + "," + str(train_score_avg_random[1]) + "," + str(train_score_avg_random[2])))
    abc_output_file.write(("\nTest (average random)," + str(test_score_avg_random[0]) + "," + str(test_score_avg_random[1]) + "," + str(test_score_avg_random[2])))
    abc_output_file.write(("\nTraining (preselected ranked best)," + str(train_score_avg_preselect[0]) + "," + str(train_score_avg_preselect[1]) + "," + str(train_score_avg_preselect[2])))
    abc_output_file.write(("\nTest (preselected ranked best)," + str(test_score_avg_preselect[0]) + "," + str(test_score_avg_preselect[1]) + "," + str(test_score_avg_preselect[2])))
    abc_output_file.write(("\nTraining (second selection best solution across runs)," + str(train_score_avg_overall_best[0]) + "," + str(train_score_avg_overall_best[1]) + "," + str(train_score_avg_overall_best[2])))
    abc_output_file.write(("\nTest (second selection best solution across runs)," + str(test_score_avg_overall_best[0]) + "," + str(test_score_avg_overall_best[1]) + "," + str(test_score_avg_overall_best[2])))

    if (do_per_run_loocv_evaluation):
        abc_output_file.write("\n\nPer-run statistics:")
        abc_output_file.write("\nStatistic,Set,Accuracy,Precision,Recall")
        abc_output_file.write("\nMEAN,Training," + str(loocv_per_run_means_train[0]) + "," + str(loocv_per_run_means_train[1]) + "," + str(loocv_per_run_means_train[2]))
        abc_output_file.write("\nMEAN,Test," + str(loocv_per_run_means_test[0]) + "," + str(loocv_per_run_means_test[1]) + "," + str(loocv_per_run_means_test[2]))
        abc_output_file.write("\nSTDEV,Training," + str(loocv_per_run_stdevs_train[0]) + "," + str(loocv_per_run_stdevs_train[1]) + "," + str(loocv_per_run_stdevs_train[2]))
        abc_output_file.write("\nSTDEV,Test," + str(loocv_per_run_stdevs_test[0]) + "," + str(loocv_per_run_stdevs_test[1]) + "," + str(loocv_per_run_stdevs_test[2]))
        abc_output_file.write("\nKOLMOGOROV-SMIRNOV NORMAL DISTRO SCORE,Training," + str(loocv_per_run_KS_train[0][0]) + "," + str(loocv_per_run_KS_train[1][0]) + "," + str(loocv_per_run_KS_train[2][0]))
        abc_output_file.write("\nKOLMOGOROV-SMIRNOV NORMAL DISTRO SCORE,Test," + str(loocv_per_run_KS_test[0][0]) + "," + str(loocv_per_run_KS_test[1][0]) + "," + str(loocv_per_run_KS_test[2][0]))
        abc_output_file.write("\nKS NORMAL DISTRO P-VALUE,Training," + str(loocv_per_run_KS_train[0][1]) + "," + str(loocv_per_run_KS_train[1][1]) + "," + str(loocv_per_run_KS_train[2][1]))
        abc_output_file.write("\nKS NORMAL DISTRO P-VALUE,Test," + str(loocv_per_run_KS_test[0][1]) + "," + str(loocv_per_run_KS_test[1][1]) + "," + str(loocv_per_run_KS_test[2][1]))
        abc_output_file.write("\nKOLMOGOROV-SMIRNOV LOG-NORMAL DISTRO SCORE,Training," + str(log_loocv_per_run_KS_train[0][0]) + "," + str(log_loocv_per_run_KS_train[1][0]) + "," + str(log_loocv_per_run_KS_train[2][0]))
        abc_output_file.write("\nKOLMOGOROV-SMIRNOV LOG-NORMAL DISTRO SCORE,Test," + str(log_loocv_per_run_KS_test[0][0]) + "," + str(log_loocv_per_run_KS_test[1][0]) + "," + str(log_loocv_per_run_KS_test[2][0]))
        abc_output_file.write("\nKS LOG-NORMAL DISTRO P-VALUE,Training," + str(log_loocv_per_run_KS_train[0][1]) + "," + str(log_loocv_per_run_KS_train[1][1]) + "," + str(log_loocv_per_run_KS_train[2][1]))
        abc_output_file.write("\nKS LOG-NORMAL DISTRO P-VALUE,Test," + str(log_loocv_per_run_KS_test[0][1]) + "," + str(log_loocv_per_run_KS_test[1][1]) + "," + str(log_loocv_per_run_KS_test[2][1]))

    abc_output_file.close()

    print("Done writing")
