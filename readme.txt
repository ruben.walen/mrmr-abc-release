mRMR-ABC-SVM gene feature selection pipeline.
Yuan Liang & Ruben Walen
Swarm Based Computing 2019-2020, professor Hao Wang

Usable with .SOFT datafiles.
Create three directories: data, working, output (configurable).
Place .SOFT in a new directory in data with the data name.
    - e.g. data/nsclc/GDS3837_full.soft
Data name and sample class names must be altered in configuration.txt
Sample class names must be derived from the sample class names in the .SOFT file.
Alter other parameters in configuration.txt, then run the pipeline:
    - extract.py
    - mRMR.py and diff_expr.py
    - ABC_selection.py
Results will be stored in working and output ('important' results in output, others in working).